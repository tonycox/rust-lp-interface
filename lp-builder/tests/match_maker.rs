use lp_builder::match_maker::data_model::{Person, Compatibility, Model};
use lp_builder::match_maker::formulate_and_solve;
use lp_modeler::solvers::Status;

fn get_persons() -> Vec<Person> {
    vec![
        Person::new("Andy".to_string()),
        Person::new("Bianca".to_string()),
        Person::new("Claire".to_string()),
        Person::new("David".to_string()),
        Person::new("Edward".to_string()),
        Person::new("Fiona".to_string()),
    ]
}

fn get_compatibilities(persons: &[Person]) -> Vec<Compatibility> {
    vec![
        Compatibility::new(&persons[0], &persons[3], 50.0),
        Compatibility::new(&persons[0], &persons[4], 75.0),
        Compatibility::new(&persons[0], &persons[5], 75.0),
        Compatibility::new(&persons[1], &persons[3], 60.0),
        Compatibility::new(&persons[1], &persons[4], 95.0),
        Compatibility::new(&persons[1], &persons[5], 80.0),
        Compatibility::new(&persons[2], &persons[3], 60.0),
        Compatibility::new(&persons[2], &persons[4], 70.0),
        Compatibility::new(&persons[2], &persons[5], 80.0),
    ]
}
#[test]
fn test_assignment() {
    let mut model = Model::new(get_persons());
    model.compatibilities = get_compatibilities(&model.persons);
    formulate_and_solve(&model);
}