use lp_builder::examples;
use lp_modeler::solvers::Status;

#[test]
fn test_simple() {
    let solver_result = examples::simple();
    assert!(solver_result.is_ok());
    assert!(solver_result.unwrap().0 == Status::Optimal);
}

#[test]
fn test_assignment() {
    examples::assignment();  // todo: update assignment() to return the solver result
}