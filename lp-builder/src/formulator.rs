use super::model;

pub enum VariableType {
    Linear,
    Integer,
    Binary,
}

pub enum ConstraintSign {
    GreaterEqual,
    LessEqual,
    Equal,
}

pub enum Objective {
    Minimise,
    Maximise,
}

pub struct DecisionVariable {
    name: String,
    objective_coefficient: f64,
    index: u64,
}

pub struct VariableEntry<'a> {
    dvar: &'a DecisionVariable,
    coefficient: f64,
    solution_value: Option<f64>,
}

pub struct Constraint<'a> {
    name: String,
    sign: ConstraintSign,
    entries: Vec<VariableEntry<'a>>,
    rhs: f64,
    index: u64,
}

pub struct ObjectiveFunction<'a> {
    objective: Objective,
    entries: Vec<VariableEntry<'a>>,
}

pub struct Formulation<'a> {
    dvars: Vec<DecisionVariable>,
    constraints: Vec<Constraint<'a>>,
    objective_function: ObjectiveFunction<'a>,
}

impl Formulation<'_> {
    pub fn new(dvars: Vec<DecisionVariable>) -> Self {
        Self {
            dvars: dvars,
            constraints: vec![],
            objective_function: ObjectiveFunction{
                objective: Objective::Minimise,
                entries: vec![]
            },
        }
    }
}

pub trait Formulator {
    fn formulate(&self) -> Formulation;
}