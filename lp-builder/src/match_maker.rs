use lp_modeler::solvers::{CbcSolver, SolverTrait};
use lp_modeler::dsl::*;
// use lp_modeler::solvers::Status;
use std::collections::HashMap;


pub mod data_model {
    use crate::formulator::*;

    #[derive(Debug, Hash, PartialEq, Eq)]
    pub struct Person {
        pub name: String,
    }

    pub struct Compatibility<'a> {
        pub lhs: &'a Person,
        pub rhs: &'a Person,
        pub compatibility_score: f64,
    }

    pub struct Model<'a> {
        pub persons: Vec<Person>,
        pub compatibilities: Vec<Compatibility<'a>>,
    }

    impl Person {
        pub fn new(name: String) -> Self {
            Self { name }
        }
    }

    impl<'a> Compatibility<'a> {
        pub fn new(lhs: &'a Person, rhs: &'a Person, compatibility_score: f64) -> Self {
            Self { lhs, rhs, compatibility_score }
        }
    }

    impl Model<'_> {
        pub fn new(persons: Vec<Person>) -> Self {
            Self {
                persons,
                compatibilities: vec![],
            }
        }
    }
}




fn get_vars<'a>(model: &'a data_model::Model) -> HashMap<(&'a data_model::Person, &'a data_model::Person), LpBinary> {
    // returns upper triangle of matrix of person v person.
    // e.g. [Katie, Helen, Mary], returns variable pairs [Katie/Helen, Katie/Mary, Helen/Mary]
    model.persons.iter().enumerate().flat_map(|(index, lhs)| {
        model.persons.iter().skip(index + 1).map(move |rhs| {
            ((lhs, rhs), LpBinary::new(&format!("{}_{}", lhs.name, rhs.name)))
        })
    }).collect()
}

fn get_objective(model: &data_model::Model, vars: &HashMap<(&data_model::Person, &data_model::Person), LpBinary>) -> Vec<LpExpression> {
    model.compatibilities.iter().map(|compatibility| {
        let var = vars.get(&(compatibility.lhs, compatibility.rhs)).unwrap();
        compatibility.compatibility_score as f32 * var
    }).collect()
}

fn get_constraints(model: &data_model::Model, vars: &HashMap<(&data_model::Person, &data_model::Person), LpBinary>) -> Vec<LpConstraint> {
    // constraints: each person must be assigned to at most one other person
    let constraints: Vec<LpConstraint> = model.persons.iter().map(|lhs| {
        let constraint_vars: Vec<&LpBinary> = model.persons.iter().filter_map(move |rhs| {
            if lhs == rhs {
                None
            } else {
                match vars.get(&(lhs, rhs)) {
                    // todo: vars should be keyed by a set so we don't have to check both sides
                    Some(var) => Some(var),
                    None => Some(vars.get(&(rhs, lhs)).unwrap())
                }
            }
        }).collect();
        lp_sum(&constraint_vars).equal(1)
    }).collect();

    constraints
}

fn get_problem(obj: &[LpExpression], constraints: &[LpConstraint]) -> LpProblem {
    let mut problem = LpProblem::new("Matchmaking", LpObjective::Maximize);
    problem.obj_expr = Some(obj.to_vec().sum());
    problem.constraints = constraints.to_vec();
    problem
}

pub fn formulate_and_solve(model: &data_model::Model) {
    println!("formulating problem...");
    let vars = get_vars(model);
    let obj = get_objective(model, &vars);
    let constraints = get_constraints(model, &vars);
    let problem = get_problem(&obj, &constraints);
    let solver = CbcSolver::new();
    println!("solving problem...");
    let result = solver.run(&problem);
    assert!(result.is_ok(), result.unwrap_err());
    let (_status, results) = result.unwrap();
    for var in vars.values() {
        println!("Var {} has value {}", var.name, results.get(&var.name).unwrap());
    }

}